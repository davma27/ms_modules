############################################################
# R3-np4 INDEX (ModMS Project)                             #
############################################################

# Non-personalized modules (alternative 4: MODifieR update modules)

############################################################
# R3.1-np4 MCODE module production                         #
############################################################

pack_R <- c("foreach", "parallel", "doParallel", "MODifieR")
for (i in 1:length(pack_R)) {
  library(pack_R[i], character.only = TRUE)
}

main_dir <- "/home/dme/Documents/ModMS_project/GSE50/NP4_basic/"

## MCODE (clique-based algorithm to identify disease modules from DEGs)

# Identification of modules with mod_mcode function
# Input: HGNC Symbols for genes (object)
#mcode_df <- as.data.frame(modifier_input_1)
mcode_module_genes <- list()
#cl = makeCluster(detectCores()-2)
#registerDoParallel(cl)
#foreach(i = 1:ncol(mcode_df), .packages = "MODifieR") %do% {
  #mcode_name <- colnames(mcode_df)[i]
  #print(mcode_name)
  #mcode_module_df <- data.frame(GS = rownames(mcode_df), P.Val = (-log10(mcode_df[, i])), stringsAsFactors = FALSE)
  #mcode_module_df <- mcode_module_df[order(mcode_module_df$P.Val, decreasing = FALSE), ]
  mcode_module_sum <- mod_mcode(MODifieR_input =  gse50_input_H3_vs_H4, ppi_network = "/home/dme/Documents/R/Master_Thesis/GSE83/MODifieR_Networks/v1/HGNC_PPi_900.txt", 
                                hierarchy      = 1,      # Hierarchical level of the PPI network to be used
                                vwp            = 0.5,    # Vertex Weight Percentage
                                haircut        = FALSE,  # Boolean, if TRUE removes singly-connected nodes from each cluster
                                fluff          = FALSE,  # Boolean, if TRUE expands cluster cores by one neighbour shell outwards, according to a density cutoff
                                fdt            = 0.8,    # Cluster density cutoff
                                loops          = TRUE,   # Boolean, if TRUE includes self-loops
                                diffgen_cutoff = 1.3,    # Threshold (expression) for genes to be considered DEGs
                                module_cutoff  = 3.5)    # Threshold (score) for output modules
  mcode_module_genes <- mcode_module_sum[[1]]$module_genes
  #names(mcode_module_genes)[i] <- mcode_name
  write.table(mcode_module_genes, paste0(main_dir, "Modifier_production_bas/mcode_results/", "mcode_H3_vs_H4", ".txt", sep = ""), sep = "\t", row.names = FALSE, quote = FALSE)
  #print(paste(mcode_name, "module has been created", sep = " "))
#}

############################################################
# R3.2 Clique Sum module production                        #
############################################################

pack_R <- c("foreach", "parallel", "doParallel", "MODifieR")
for (i in 1:length(pack_R)) {
  library(pack_R[i], character.only = TRUE)
}

main_dir <- "/home/dme/Documents/ModMS_project/GSE50/NP4_basic/"

## Clique Sum (clique-based algorithm to identify disease SUsceptibility Modules from DEGs)

# Identification of modules with clique_sum function
# Input: HGNC Symbols for genes (object) and network (path)
#cliquesum_df <- modifier_input_1
cliquesum_module_genes <- list()
#cl = makeCluster(detectCores()-2)
#registerDoParallel(cl)
#foreach(i = 605:ncol(cliquesum_df), .packages = "MODifieR") %do% {
  #cliquesum_name <- colnames(cliquesum_df)[i]
  #print(cliquesum_name)
  #cliquesum_module_df <- data.frame(GS = rownames(cliquesum_df), P.Val = (-log10(cliquesum_df[, i])), stringsAsFactors = FALSE)
  #cliquesum_module_df <- cliquesum_module_df[order(cliquesum_module_df$P.Val, decreasing = TRUE), ]
  cliquesum_module_sum <- clique_sum(MODifieR_input =  gse50_input_H3_vs_H4, ppi_network = "/home/dme/Documents/R/Master_Thesis/GSE83/MODifieR_Networks/v1/HGNC_PPi_900.txt", 
                                     simplify_graph  = TRUE,   # Boolean, if TRUE simplifies the graph
                                     n_iter          = 10000,  # Number of iterations to be performed
                                     cutoff          = 0.05,   # Threshold (p-value)
                                     min_clique_size = 5,      # Minimal clique size
                                     min_deg_clique  = 3)      # Minimal degree of the clique
  cliquesum_module_genes <- cliquesum_module_sum$module_genes
  #names(cliquesum_module_genes)[i] <- cliquesum_name
  write.table(cliquesum_module_genes, paste0(main_dir, "Modifier_production_bas/cliquesum_results/", "cliquesum_H3_vs_H4", ".txt", sep = ""), sep = "\t", row.names = FALSE, quote = FALSE)
  #print(paste(cliquesum_name, "module has been created", sep = " "))
#}

cliquesum_module_genes_entrez <- AnnotationDbi::select(org.Hs.eg.db, key = as.character(as.vector(cliquesum_module_genes)),
                                                       columns = c("ENTREZID", "SYMBOL"),
                                                       keytype = "SYMBOL")
write.table(cliquesum_module_genes_entrez[, 2], paste0(main_dir, "Modifier_production_bas/cliquesum_results/", "cliquesum_H3_vs_H4", ".txt", sep = ""), sep = "\t", row.names = FALSE, quote = FALSE)  

############################################################
# R3.3 DIAMOnD module production                           #
############################################################

pack_R <- c("foreach", "parallel", "doParallel", "MODifieR")
for (i in 1:length(pack_R)) {
  library(pack_R[i], character.only = TRUE)
}

main_dir <- "/home/dme/Documents/ModMS_project/GSE50/NP4_basic/"

## DIAMOnD (seed gene-based algorithm to identify disease modules from DEGs)

# Identification of modules with diamond function
# Input: Entrez IDs for genes (path) and network (path)
#diamond_df <- modifier_input_2
diamond_module_genes <- list()
#cl = makeCluster(detectCores()-2)
#registerDoParallel(cl)
#foreach(i = 1:ncol(diamond_df), .packages = "MODifieR") %do% {
  #diamond_name <- colnames(diamond_df)[i]
  #print(diamond_name)
  #diamond_module_df <- data.frame(GS = rownames(diamond_df), P.Val = (-log10(diamond_df[, i])), stringsAsFactors = FALSE)
  #diamond_module_df <- diamond_module_df[order(diamond_module_df$P.Val, decreasing = FALSE), ]
  #write.table(diamond_module_df[1:2000, 1], file = paste0(main_dir, "Modifier_production/diamond_module_df.txt"), col.names = FALSE, sep = "\t", dec = ".", row.names = FALSE, quote = FALSE)
  diamond_module_sum <- diamond(MODifieR_input = gse50_input_H3_vs_H4, ppi_network = "/home/dme/Documents/R/Master_Thesis/GSE83/MODifieR_Networks/v1/Entrez_PPi.txt", 
                                n_output_genes = 200,   # Maximum number of genes to include in the final module
                                seed_weight    = 10,    # Weight assignation for seed genes
                                include_seed   = TRUE)  # Boolean, if TRUE includes seed genes in the final module
  diamond_module_genes <- diamond_module_sum$module_genes
  #names(diamond_module_genes)[i] <- diamond_name
  write.table(diamond_module_genes, paste0(main_dir, "Modifier_production_bas/diamond_results/", "diamond_H3_vs_H4", ".txt", sep = ""), sep = "\t", row.names = FALSE, quote = FALSE)
  #print(paste(diamond_name, "module has been created", sep = " "))
#}
  