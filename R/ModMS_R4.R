############################################################
# R4 INDEX (ModMS Project)                                 #
############################################################

# R4.1 Creation of module storage directory structure
# R4.2 Importation of MODifieR modules 
# R4.3 Creation of sample classifier
# R4.4 Production of module combinations by categories

############################################################
# R4.1 Creation of module storage directory structure      #
############################################################

main_dir <- "/home/dme/Documents/ModMS_project/GSE50/"

# Create required directories for module combinations and sample classification
new_sub_dir <- c("MC", "CS", "D", "MC_U_CS", "MC_I_CS", "MC_U_D", "MC_I_D", "CS_U_D", "CS_I_D", 
                 "MC_U_CS_U_D", "MC_U_CS_I_D", "MC_U_D_I_CS", "CS_U_D_I_MC", "MC_I_CS_I_D")
new_sub_dir2 <- c("MS_T_BL", "MS_T_Y1", "MS_T_Y2", "MS_U_BL", "MS_U_Y1", "MS_U_Y2", "C")
dir.create(path = paste0(main_dir, "Modifier_combinations"), showWarnings = FALSE)
for (i in 1:length(new_sub_dir)) {
  new_folder <- dir.create(paste0(main_dir, "Modifier_combinations/", new_sub_dir[i]), showWarnings = FALSE)
  for (j in 1:length(new_sub_dir2)) {
  new_folder <- dir.create(paste0(main_dir, "Modifier_combinations/", new_sub_dir[i], "/", new_sub_dir2[j]), showWarnings = FALSE)
  }
}

############################################################
# R4.2 Importation of MODifieR modules                     #
############################################################

# Selection of module origin ("local" or "cluster")
import_sel <- "local" 

if (import_sel == "local") {
  filelist_MC <- list.files(path = paste0(main_dir, "Modifier_production/mcode_results/"), pattern = "*.txt")
  filedir_MC <- paste0(main_dir, "Modifier_production/mcode_results/", filelist_MC)
  datalist_MC <- lapply(filedir_MC, FUN = read.table, header = TRUE)
  names_MC <- substr(filelist_MC, 1, 10)
  names(datalist_MC) <- names_MC

  filelist_CS <- list.files(path = paste0(main_dir, "Modifier_production/cliquesum_results/"), pattern = "*.txt")
  filedir_CS <- paste0(main_dir, "Modifier_production/cliquesum_results/", filelist_CS)
  datalist_CS <- lapply(filedir_CS, FUN = read.table, header = TRUE)
  names_CS <- substr(filelist_CS, 1, 10)
  names(datalist_CS) <- names_CS

  filelist_D <- list.files(path = paste0(main_dir, "Modifier_production/diamond_results/"), pattern = "*.txt")
  filedir_D <- paste0(main_dir, "Modifier_production/diamond_results/", filelist_D)
  datalist_D <- lapply(filedir_D, FUN = read.table, header = TRUE)
  names_D <- substr(filelist_D, 1, 10)
  names(datalist_D) <- names_D
}

if (import_sel == "cluster") { 
  filelist_MC <- list.files(path = paste0(main_dir, "Modifier_production_gamma/mcode_results/"), pattern = "*.txt")
  filedir_MC <- paste0(main_dir, "Modifier_production_gamma/mcode_results/", filelist_MC)
  datalist_MC <- lapply(filedir_MC, FUN = read.table, header = TRUE)
  names_MC <- substr(filelist_MC, 1, 10)
  names(datalist_MC) <- names_MC

  filelist_CS <- list.files(path = paste0(main_dir, "Modifier_production_gamma/cliquesum_results/"), pattern = "*.txt")
  filedir_CS <- paste0(main_dir, "Modifier_production_gamma/cliquesum_results/", filelist_CS)
  datalist_CS <- lapply(filedir_CS, FUN = read.table, header = TRUE)
  names_CS <- substr(filelist_CS, 1, 10)
  names(datalist_CS) <- names_CS

  filelist_D <- list.files(path = paste0(main_dir, "Modifier_production_gamma/diamond_results/"), pattern = "*.txt")
  filedir_D <- paste0(main_dir, "Modifier_production_gamma/diamond_results/", filelist_D)
  datalist_D <- lapply(filedir_D, FUN = read.table, header = TRUE)
  names_D <- substr(filelist_D, 1, 10)
  names(datalist_D) <- names_D
}

############################################################
# R4.3 Creation of sample classifier                       #
############################################################

sample_classifier <- matrix(data = NA, nrow = nrow(phenosum), ncol = 2)
colnames(sample_classifier) <- c("Sample", "Category")
sample_classifier[, 1] <- phenosum[, 1]
for (i in 1:nrow(phenosum)) {
  if (phenosum[i, 2] == "disease: multiple sclerosis") {
    sample_classifier[i, 2] <- "MS_"
    if (phenosum[i, 4] == "treatment: untreated") {
      sample_classifier[i, 2] <- paste0(sample_classifier[i, 2], "U_")
      } else { sample_classifier[i, 2] <- paste0(sample_classifier[i, 2], "T_")}
    if (phenosum[i, 3] == "visit: Baseline") {
      sample_classifier[i, 2] <- paste0(sample_classifier[i, 2], "BL")
      } else { if (phenosum[i, 3] == "visit: follow-up year 1") {
        sample_classifier[i, 2] <- paste0(sample_classifier[i, 2], "Y1")
      } else { if (phenosum[i, 3] == "visit: follow-up year 2") {
        sample_classifier[i, 2] <- paste0(sample_classifier[i, 2], "Y2")
        } else { print("An error ocurred in the classification")}
      }
      }
  } else { sample_classifier[i, 2] <- "C"
  }
}
sample_classifier <- as.data.frame(sample_classifier)

# Classification of sample names as MS / C
names_MS <- as.character(sample_classifier[!sample_classifier$Category == "C" , 1])
names_C <- as.character(sample_classifier[sample_classifier$Category == "C" , 1])
names_output <- c(names_MS, names_C)

############################################################
# R4.4 Production of module combinations by categories     #
############################################################

comb_sel <- c("MC", "CS", "D", "MC_I_CS", "MC_U_CS", "MC_I_D", 
              "MC_U_D", "CS_I_D", "CS_U_D", "MC_I_CS_I_D", 
              "MC_U_CS_U_D", "MC_U_CS_I_D", "MC_U_D_I_CS", 
              "CS_U_D_I_MC")
                                      # MC = MCODE
                                      # CS = Clique Sum
                                      # D = DIAMOnD
                                      # MC_I_CS = Intersection MCODE & Clique Sum
                                      # MC_U_CS = Union MCODE & Clique Sum
                                      # MC_I_D = Intersection MCODE & DIAMOnD
                                      # MC_U_D = Union MCODE & DIAMOnD
                                      # CS_I_D = Intersection Clique Sum & DIAMOnD
                                      # CS_U_D = Union Clique Sum & DIAMOnD
                                      # MC_I_CS_I_D = Intersection MCODE & Clique Sum & DIAMOnD
                                      # MC_U_CS_U_D = Union MCODE & Clique Sum & DIAMOnD
                                      # MC_U_CS_I_D = Intersection (Union MCODE & Clique Sum) & DIAMOnD
                                      # MC_U_D_I_CS = Intersection (Union MCODE & DIAMOnD) & Clique Sum
                                      # CS_U_D_I_MC = Intersection (Union Clique Sum & DIAMOnD) & MCODE

for (name in comb_sel) {
for (i in 1:nrow(sample_classifier)) {
  #print("------------------------------------------------------------")
  #print(names_output[i])
  sample_GSM <- names_output[i]
  sample_MC <- t(as.data.frame(subset(datalist_MC, names(datalist_MC) == sample_GSM)))
  sample_CS <- t(as.data.frame(subset(datalist_CS, names(datalist_CS) == sample_GSM)))
  sample_D <- t(as.data.frame(subset(datalist_D, names(datalist_D) == sample_GSM)))
  if (name == "MC") {
    mod_comb <- sample_MC
  } else if (name == "CS") {
    mod_comb <- sample_CS
  } else if (name == "D") {
    mod_comb <- sample_D
  } else if (name == "MC_I_CS") {
    mod_comb <- t(as.matrix(intersect(sample_MC, sample_CS)))
    rownames(mod_comb) <- sample_GSM
  } else if (name == "MC_U_CS") {
    mod_comb <- t(as.matrix(union(sample_MC, sample_CS)))
    rownames(mod_comb) <- sample_GSM
  } else if (name == "MC_I_D") {
    mod_comb <- t(as.matrix(intersect(sample_MC, sample_D)))
    rownames(mod_comb) <- sample_GSM
  } else if (name == "MC_U_D") {
    mod_comb <- t(as.matrix(union(sample_MC, sample_D)))
    rownames(mod_comb) <- sample_GSM
  } else if (name == "CS_I_D") {
    mod_comb <- t(as.matrix(intersect(sample_CS, sample_D)))
    rownames(mod_comb) <- sample_GSM
  } else if (name == "CS_U_D") {
    mod_comb <- t(as.matrix(union(sample_CS, sample_D)))
    rownames(mod_comb) <- sample_GSM
  } else if (name == "MC_I_CS_I_D") {
    mod_comb_b <- intersect(sample_MC, sample_CS)
    mod_comb <- t(as.matrix(intersect(mod_comb_b, sample_D)))
    rownames(mod_comb) <- sample_GSM
  } else if (name == "MC_U_CS_U_D") {
    mod_comb_b <- union(sample_MC, sample_CS)
    mod_comb <- t(as.matrix(union(mod_comb_b, sample_D)))
    rownames(mod_comb) <- sample_GSM
  } else if (name == "MC_U_CS_I_D") {
    mod_comb_b <- union(sample_MC, sample_CS)
    mod_comb <- t(as.matrix(intersect(mod_comb_b, sample_D)))
    rownames(mod_comb) <- sample_GSM
  } else if (name == "MC_U_D_I_CS") {
    mod_comb_b <- union(sample_MC, sample_D)
    mod_comb <- t(as.matrix(intersect(mod_comb_b, sample_CS)))
    rownames(mod_comb) <- sample_GSM
  } else if (name == "CS_U_D_I_MC") {
    mod_comb_b <- union(sample_CS, sample_D)
    mod_comb <- t(as.matrix(intersect(mod_comb_b, sample_MC)))
    rownames(mod_comb) <- sample_GSM
  } else {
    print(paste0("An error was found for ", sample_GSM))
  }
  rownames(mod_comb)[1] <- sample_GSM
  assign(paste("mod_comb_", sample_GSM, sep = ""), mod_comb)
  
  if ((sample_classifier$Category[which(sample_classifier$Sample %in% sample_GSM)] == "MS_T_BL") == TRUE) {
    #print("Sample category: [MS patient] [Treated] [Baseline]")
    mod_comb_name <- paste0(main_dir, "Modifier_combinations/", name, "/MS_T_BL/", sample_GSM, ".txt")
    
  } else { if ((sample_classifier$Category[which(sample_classifier$Sample %in% sample_GSM)] == "MS_T_Y1") == TRUE) {
    #print("Sample category: [MS patient] [Treated] [Year 1]")
    mod_comb_name <- paste0(main_dir, "Modifier_combinations/", name, "/MS_T_Y1/", sample_GSM, ".txt")
    
  } else { if ((sample_classifier$Category[which(sample_classifier$Sample %in% sample_GSM)] == "MS_T_Y2") == TRUE) {
    #print("Sample category: [MS patient] [Treated] [Year 2]")  
    mod_comb_name <- paste0(main_dir, "Modifier_combinations/", name, "/MS_T_Y2/", sample_GSM, ".txt")
    
  } else { if ((sample_classifier$Category[which(sample_classifier$Sample %in% sample_GSM)] == "MS_U_BL") == TRUE) {
    #print("Sample category: [MS patient] [Untreated] [Baseline]")   
    mod_comb_name <- paste0(main_dir, "Modifier_combinations/", name, "/MS_U_BL/", sample_GSM, ".txt")
    
  } else { if ((sample_classifier$Category[which(sample_classifier$Sample %in% sample_GSM)] == "MS_U_Y1") == TRUE) {
    #print("Sample category: [MS patient] [Untreated] [Year 1]")
    mod_comb_name <- paste0(main_dir, "Modifier_combinations/", name, "/MS_U_Y1/", sample_GSM, ".txt")
    
  } else { if ((sample_classifier$Category[which(sample_classifier$Sample %in% sample_GSM)] == "MS_U_Y2") == TRUE) {
    #print("Sample category: [MS patient] [Untreated] [Year 2]") 
    mod_comb_name <- paste0(main_dir, "Modifier_combinations/", name, "/MS_U_Y2/", sample_GSM, ".txt")
    
  } else { if ((sample_classifier$Category[which(sample_classifier$Sample %in% sample_GSM)] == "C") == TRUE) {
    #print("Sample category: [Control]") 
    mod_comb_name <- paste0(main_dir, "Modifier_combinations/", name, "/C/", sample_GSM, ".txt")
  } } } } } } }  
  write.table(mod_comb, file = mod_comb_name, row.names = TRUE, col.names = FALSE, quote = FALSE, sep = " ", dec = ".")
  #print(paste0("Module created for ", sample_GSM, " for the combination ", name))
 }
}

# Cleaning environment
rm(list = ls(pattern = "mod_comb_"))
